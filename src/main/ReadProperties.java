package main;

import java.io.FileInputStream;
import java.util.Properties;

public class ReadProperties {
	Properties prop;
	public ReadProperties(String file) {
		try {
			FileInputStream is = new FileInputStream(file);
			prop = new Properties();
			prop.load(is);
					
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void print(String data) {
		String value = prop.getProperty(data);
		
		System.out.println(data +": "+ value);
	}
}
