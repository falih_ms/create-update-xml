package main;

import java.util.Scanner;

public class Main {
	static 	Scanner scan = new Scanner(System.in);
	static XmlReader reader;
	static ReadProperties prop; 
	
	public static void main(String[] args) {
		int opt = 0;
		reader = new XmlReader("./resource/example.xml", "breakfast_menu");		
		prop = new ReadProperties("./resource/config.properties");
		printMenu();
	}
	
	public static void printMenu() {
		int opt = 0;
		do {
			System.out.println("XML Reader");
			System.out.println("================");
			System.out.println();
			System.out.println("1. View properties file");
			System.out.println("2. View xml");
			System.out.println("3. Edit xml");
			System.out.println("4. Exit");
			System.out.print("opt:");
			opt = scan.nextInt();
			scan.nextLine();
			if(opt==1) {
				prop.print("username");
				prop.print("password");
			}else if(opt==2) {
				reader.print();
			}else if(opt == 3) {
				updateXml();
			}else if(opt == 4) {
				System.out.println("Thank You");
			}
		} while (opt != 4);

	}
	
	public static void updateXml() {
		
		System.out.println("Select tag that want to update");
		System.out.print("Tag:");
		String tag = scan.nextLine();
		
		reader.getTag(tag);
		
		System.out.println("Select index that want to update");
		System.out.print("Index:");
		int index = scan.nextInt();
		scan.nextLine();
		
		System.out.println("Input text to update");
		System.out.print("text:");		
		String text = scan.nextLine();
		reader.update(index, tag, text);
		
		
		
	}

}
