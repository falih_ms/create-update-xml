package main;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;


public class XmlReader {
	DocumentBuilderFactory factory;
	DocumentBuilder builder;
	Document doc;
	NodeList StartingList;
	private int tab;
	private String file;
	private String startingTag;
	
	public XmlReader(String file, String startingTag) {	
		this.file = file;
		this.startingTag = startingTag;
		factory = DocumentBuilderFactory.newInstance();
		factory.setIgnoringComments(true);		
	}
	
	public void print() {
		try {
			builder = factory.newDocumentBuilder();
			doc =  builder.parse(file);
			StartingList = doc.getElementsByTagName(startingTag);
			tab = -1;
			getChild(StartingList);	
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void getChild(NodeList nl) {
		boolean flag = false;
		tab++;
		for (int i = 0; i < nl.getLength(); i++) {
			Node n = nl.item(i);
			if(n.getNodeType() == Node.ELEMENT_NODE) {
				Element e = (Element) n;
				NodeList cl = e.getChildNodes();
				tab();
				System.out.print("<" + e.getTagName() + ">");
				for (int j = 0; j < cl.getLength(); j++) {
					Node cn = cl.item(j);
					if(cn.getNodeType() == Node.ELEMENT_NODE) {
						flag = true;
					}
				}
				if(flag) {
					System.out.println();
					getChild(cl);
					tab();
				}else {
					System.out.print(e.getTextContent());
				}
				System.out.println("</" + e.getTagName() + ">");

			}
		}
		tab--;
	}
	
	public void tab() {
		for (int j = 0; j < tab; j++) {
			System.out.print(" ");
		}
	}
	
	
	public void update(int index, String tag, String text) {
		NodeList nl = doc.getElementsByTagName(tag);
		for (int i = 0; i < nl.getLength(); i++) {
			Node n = nl.item(i);
			Element e = (Element) n;
			if(i == index-1) {
				n.setTextContent(text);
			}
		}
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(file));
			transformer.transform(source, result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("Done");
	}
	
	public void getTag(String tag) {
		NodeList nl = doc.getElementsByTagName(tag);
		for (int i = 0; i < nl.getLength(); i++) {
			Node n = nl.item(i);
			Element e = (Element) n;
			System.out.println((i+1)+ ". "+e.getTextContent());
		}
	}
		
}

